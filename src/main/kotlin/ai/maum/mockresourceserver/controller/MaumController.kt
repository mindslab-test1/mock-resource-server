package ai.maum.mockresourceserver.controller

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class MaumController {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    @GetMapping("/jwt-info:get")
    fun getJwtInfo(@AuthenticationPrincipal jwt: Jwt): String {
        logger.info("$jwt")
        return String.format("Mock Resource Server Response %s", jwt.tokenValue)
    }

    @GetMapping("/scope:exist")
    fun isScopeExist() = "You Have a maum-api-tts-use scope"

}
package ai.maum.mockresourceserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MockResourceServerApplication

fun main(args: Array<String>) {
    runApplication<MockResourceServerApplication>(*args)
}
